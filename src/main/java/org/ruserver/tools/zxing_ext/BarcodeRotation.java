package org.ruserver.tools.zxing_ext;

public enum  BarcodeRotation {

    ROTATION_0(0),

    ROTATION_90(90),

    ROTATION_180(180),

    ROTATION_270(270);

    private final double angle;

    BarcodeRotation(double angle) {
        this.angle = angle;
    }

    public double getAngle() {
        return angle;
    }
}
