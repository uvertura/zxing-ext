package org.ruserver.tools.zxing_ext;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public class BarcodeBuilder {
    private static final Color DEFAULT_BG_COLOR = Color.WHITE;
    private static final Color DEFAULT_FG_COLOR = Color.BLACK;
    private static final Color DEFAULT_TEXT_COLOR = Color.BLACK;
    private static final TextPosition DEFAULT_TEXT_POSITION = TextPosition.BOTTOM;
    private static final BarcodeRotation DEFAULT_ROTATION = BarcodeRotation.ROTATION_0;


    private final BarcodeFormat barcodeFormat;

    private final String text;

    private final Writer writer;

    private Color backgroundColor = DEFAULT_BG_COLOR;

    private Color foregroundColor = DEFAULT_FG_COLOR;

    private Color textColor = DEFAULT_TEXT_COLOR;

    private int width = 300;

    private int height = 300;

    private TextPosition textPosition = DEFAULT_TEXT_POSITION;

    private BarcodeRotation barcodeRotation = DEFAULT_ROTATION;

    private BarcodeBuilder(final BarcodeFormat barcodeFormat, final String text) {
        this.barcodeFormat = barcodeFormat;
        this.text = text;
        this.writer = BarcodeFormatUtil.getWriter(barcodeFormat);


    }

    public static BarcodeBuilder newBuilder(final BarcodeFormat barcodeFormat, final String text) {
        return new BarcodeBuilder(barcodeFormat, text);
    }

    public BarcodeBuilder setBackgroundColor(final Color backgroundColor) {
        this.backgroundColor = (null == backgroundColor ? DEFAULT_BG_COLOR : backgroundColor);
        return this;
    }

    public BarcodeBuilder setForegroundColor(final Color foregroundColor) {
        this.foregroundColor = (null == foregroundColor ? DEFAULT_FG_COLOR : foregroundColor);
        return this;
    }

    public BarcodeBuilder setTextPosition(final TextPosition textPosition) {
        this.textPosition = (null == textPosition ? DEFAULT_TEXT_POSITION : textPosition);
        return this;
    }

    public BarcodeBuilder setTextColor(final Color textColor) {
        this.textColor = (null == textColor ? DEFAULT_TEXT_COLOR : textColor);
        return this;
    }

    public BarcodeBuilder setRotation(final BarcodeRotation rotation) {
        this.barcodeRotation = (null == rotation ? DEFAULT_ROTATION : rotation);
        return this;
    }

    public BarcodeBuilder setHeight(int height) {
        this.height = height;
        return this;
    }

    public BarcodeBuilder setWidth(int width) {
        this.width = width;
        return this;
    }

    public BufferedImage build() throws WriterException {
        final var bitMatrix = writer.encode(text, barcodeFormat, width, height);
        final var image = MatrixToImageWriter.toBufferedImage(bitMatrix,
                new MatrixToImageConfig(foregroundColor.getRGB(), backgroundColor.getRGB()));

        if (TextPosition.NONE != textPosition && BarcodeFormatUtil.isOneDimension(barcodeFormat))
            addText(image);

        if (BarcodeRotation.ROTATION_0 != barcodeRotation)
            return rotate(image);

        return image;
    }

    protected void addText(BufferedImage image) {
        final var g = image.createGraphics();

        g.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        final int fontSize = (int) Math.floor(image.getWidth() / text.length() * 1.5);
        final Font font = new Font("Arial", Font.BOLD, fontSize);
        g.setFont(font);

        final var fontMetrics = g.getFontMetrics();

        final int startYPos;
        switch (textPosition) {
            case TOP:
                startYPos = fontMetrics.getHeight();
                break;
            default:
                startYPos = image.getHeight();
        }

        g.setColor(backgroundColor);
        g.fillRect(0, (startYPos - fontMetrics.getHeight()), image.getWidth(), fontMetrics.getHeight());


        int startPos = (image.getWidth() - fontMetrics.stringWidth(text)) / 2;
        if (0 > startPos)
            startPos = 0;

        g.setColor(textColor);
        g.drawString(text, startPos, startYPos - (font.getSize() / 4));
        g.dispose();
    }


    protected BufferedImage rotate(BufferedImage image) {
        int w = image.getWidth(), h = image.getHeight();

        if (BarcodeRotation.ROTATION_90 == barcodeRotation || BarcodeRotation.ROTATION_270 == barcodeRotation) {
            w = image.getHeight();
            h = image.getWidth();
        }

        BufferedImage rotated = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = rotated.createGraphics();

        AffineTransform at = new AffineTransform();
        at.translate((w - image.getWidth()) / 2, (h - image.getHeight()) / 2);
        at.rotate(Math.toRadians(barcodeRotation.getAngle()), image.getWidth() / 2, image.getHeight() / 2);
        g.drawImage(image, at, null);
        g.dispose();
        return rotated;
    }

}
