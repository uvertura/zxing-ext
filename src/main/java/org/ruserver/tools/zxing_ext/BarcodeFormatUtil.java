package org.ruserver.tools.zxing_ext;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Reader;
import com.google.zxing.Writer;
import com.google.zxing.aztec.AztecReader;
import com.google.zxing.aztec.AztecWriter;
import com.google.zxing.datamatrix.DataMatrixReader;
import com.google.zxing.datamatrix.DataMatrixWriter;
import com.google.zxing.maxicode.MaxiCodeReader;
import com.google.zxing.oned.*;
import com.google.zxing.oned.rss.RSS14Reader;
import com.google.zxing.oned.rss.expanded.RSSExpandedReader;
import com.google.zxing.pdf417.PDF417Reader;
import com.google.zxing.pdf417.PDF417Writer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;

import java.security.InvalidParameterException;

public final class BarcodeFormatUtil {

    private BarcodeFormatUtil() {
        throw new RuntimeException("Forbidden create instance of Util class.");
    }

    public static Writer getWriter(final BarcodeFormat barcodeFormat) {
        switch (barcodeFormat) {
            case AZTEC:
                return new AztecWriter();
            case CODABAR:
                return new CodaBarWriter();
            case CODE_39:
                return new Code39Writer();
            case CODE_93:
                return new Code93Writer();
            case CODE_128:
                return new Code128Writer();
            case DATA_MATRIX:
                return new DataMatrixWriter();
            case EAN_8:
                return new EAN8Writer();
            case EAN_13:
                return new EAN13Writer();
            case ITF:
                return new ITFWriter();
            case PDF_417:
                return new PDF417Writer();
            case QR_CODE:
                return new QRCodeWriter();
            case UPC_A:
                return new UPCAWriter();
            case UPC_E:
                return new UPCEWriter();

            case MAXICODE:
            case RSS_14:
            case RSS_EXPANDED:
            case UPC_EAN_EXTENSION:
            default:
                throw new InvalidParameterException("Unsupported BarcodeFormat.");
        }

    }

    public static Reader getReader(final BarcodeFormat barcodeFormat) {
        switch (barcodeFormat) {
            case AZTEC:
                return new AztecReader();
            case CODABAR:
                return new CodaBarReader();
            case CODE_39:
                return new Code39Reader();
            case CODE_93:
                return new Code93Reader();
            case CODE_128:
                return new Code128Reader();
            case DATA_MATRIX:
                return new DataMatrixReader();
            case EAN_8:
                return new EAN8Reader();
            case EAN_13:
                return new EAN13Reader();
            case ITF:
                return new ITFReader();
            case PDF_417:
                return new PDF417Reader();
            case QR_CODE:
                return new QRCodeReader();
            case UPC_A:
                return new UPCAReader();
            case UPC_E:
                return new UPCEReader();
            case MAXICODE:
                return new MaxiCodeReader();
            case RSS_14:
                return new RSS14Reader();
            case RSS_EXPANDED:
                return new RSSExpandedReader();

            case UPC_EAN_EXTENSION:
            default:
                throw new InvalidParameterException("Unsupported BarcodeFormat.");
        }
    }

    public static boolean isOneDimension(final BarcodeFormat barcodeFormat){
        switch (barcodeFormat) {
            case CODE_39:
            case CODE_93:
            case CODE_128:
            case UPC_A:
            case UPC_E:
            case CODABAR:
            case EAN_8:
            case EAN_13:
            case ITF:
            case RSS_14:
            case RSS_EXPANDED:
            case UPC_EAN_EXTENSION:
                return true;

            case AZTEC:
            case DATA_MATRIX:
            case PDF_417:
            case QR_CODE:
            case MAXICODE:
                return false;
            default:
                throw new InvalidParameterException("Unsupported BarcodeFormat.");
        }
    }

    public static boolean isTwoDimension(final BarcodeFormat barcodeFormat){
        return !isOneDimension(barcodeFormat);
    }

}
