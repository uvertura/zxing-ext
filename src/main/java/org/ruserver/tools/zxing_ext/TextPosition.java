package org.ruserver.tools.zxing_ext;

public enum TextPosition {

    NONE,

    TOP,

    BOTTOM

}
