package org.ruserver.tools.zxing_ext;

import com.google.zxing.BarcodeFormat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.FileOutputStream;

class BarcodeBuilderTest {

    @BeforeEach
    void init(){
    }

    @Test
    void setBackgroundColor() {
    }

    @Test
    void setForegroundColor() {
    }

    @Test
    void build() throws Exception{

        final var builder = BarcodeBuilder.newBuilder(BarcodeFormat.CODE_128, "258465325845")
                .setTextPosition(TextPosition.TOP)
                .setRotation(BarcodeRotation.ROTATION_90)
                .setHeight(70)
                .setWidth(150)
                .setTextColor(Color.RED)
                .setForegroundColor(Color.BLUE)
                .setBackgroundColor(Color.GREEN);
        final var image = builder.build();
        try(final var os = new FileOutputStream("C:\\temp\\test.png")){
            ImageIO.write(image, "png", os);
        }

    }
}